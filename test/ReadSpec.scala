package datastore.test

import org.specs2.mutable._
import org.specs2.runner._
import org.junit.runner._
import datastore.models._
import scala.collection.mutable.MutableList

@RunWith(classOf[JUnitRunner])
class ReadSpec extends Specification {

  val DatasetId = "datastore-scala"

  "Google cloud datastore" should {
    "find a entity entity" in {
      import Namespaces.Default
      val store = GoogleDatastore(DatasetId)
      val key = store.create("Users", "name" -> "Dorel")
      store.findOne("Users", key) must beSome
      store.findOne("Users", key).get("name") must equalTo("Dorel")
    }
    "return none for nonexistent entity" in {
      import Namespaces.Default
      val store = GoogleDatastore(DatasetId)
      store.findOne("Users", "notthere") must beNone
    }
    "return none for existent entity in other namespace" in {
      import Namespaces.Default
      val store = GoogleDatastore(DatasetId)
      val id = store.create("Users", "name" -> "Dorel")
      store.findOne("Users", id) must beSome
      store.findOne("Users", id)(Default) must beSome
      store.findOne("Users", id)(Namespace("notdefault")) must beNone
    }
    "return result for existent entity in namespace other than default" in {
      val store = GoogleDatastore(DatasetId)
      implicit val ns = Namespace("specific")
      val id = store.create("Users", "name" -> "Dorel")
      store.findOne("Users", id)(ns) must beSome
      store.findOne("Users", id)(Namespace("notdefault")) must beNone
      store.findOne("Users", id)(Namespaces.Default) must beNone
    }
    "correctly find all entities" in {
      val store = GoogleDatastore(DatasetId)
      val Users = "Users"
      implicit val ns = Namespace("beta")
      store.remEntities(Users, store.findAll(Users))
      store.findAll(Users).size must equalTo(0)
      val ent = store.create(Users, store.IdProperty -> "a1234", "name" -> "Dorel")
      try {
        store.findAll(Users).size must equalTo(1)
      } finally {
        store.remEntities(Users, store.findAll(Users))
      }
    }
  }
}
