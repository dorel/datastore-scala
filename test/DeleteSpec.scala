package datastore.test

import org.specs2.mutable._
import org.specs2.runner._
import org.junit.runner._
import datastore.models._

@RunWith(classOf[JUnitRunner])
class DeleteSpec extends Specification {

  val DatasetId = "datastore-scala"

  "Google cloud datastore" should {
    "correctly delete an entity from the default namespace" in {
      import Namespaces.Default
      val Users = "Users"
      val store = GoogleDatastore(DatasetId)
      val ent = store.create(Users, "name" -> "Dorel")
      Option(ent) must beSome
      store.findOne(Users, ent) must beSome
      store.remove(Users, Seq(ent))
      store.findOne(Users, ent) must beNone
    }
    "correctly delete an entity from a custom namespace" in {
      implicit val ns = Namespace("tst")
      val Users = "Users"
      val store = GoogleDatastore(DatasetId)
      val ent = store.create(Users, "name" -> "Dorel")
      Option(ent) must beSome
      store.findOne(Users, ent) must beSome
      store.remove(Users, Seq(ent))
      store.findOne(Users, ent) must beNone
    }
    "correctly delete an entity with provided ID" in {
      implicit val ns = Namespace("tst")
      val Users = "Users"
      val store = GoogleDatastore(DatasetId)
      val ent = store.create(Users, "name" -> "Dorel", store.IdProperty -> "aaa")
      Option(ent) must beSome("aaa")
      store.findOne(Users, ent) must beSome
      store.remove(Users, Seq(ent))
      store.findOne(Users, ent) must beNone
    }
    "correctly deletes in 500 sized batches" in {
      implicit val ns = Namespace("tst")
      val Users = "Users"
      val store = GoogleDatastore(DatasetId)
      store.remEntities(Users, store.findAll(Users))
      store.findAll(Users).size must equalTo(0)
      def op = store.create(Users, "name" -> "Dorel")
      batchOf100(op, 7)
      store.findAll(Users).size must equalTo(700)
      store.remEntities(Users, store.findAll(Users))
      for(i <- 1 to 15) {//give it time to delete
        store.findAll(Users)
      }
      store.findAll(Users).size must equalTo(0)
    }
  }

  import scala.concurrent._
  import scala.concurrent.ExecutionContext.Implicits._
  import scala.concurrent.duration._
  import scala.language.postfixOps
  private def batchOf100(fn: => Any, batchesNo: Int) = {
    val store = GoogleDatastore("oauthorize-00")
    val AccessTokens = "access_tokens"
    implicit val ns = Namespace("beta")
    def batch(v: Long) = {
      (1 to 100) map { i =>
        Future {
          fn
        }
      }
    }
    val c = System.currentTimeMillis
    for (b <- c to c + batchesNo - 1) {
      val f = Future.sequence(batch(b))
      Await.result(f, Duration(10, "seconds"))
    }
  }
}
