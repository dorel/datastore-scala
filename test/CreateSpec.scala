package datastore.test

import org.specs2.mutable._
import org.specs2.runner._
import org.junit.runner._
import datastore.models._
import java.util.UUID

@RunWith(classOf[JUnitRunner])
class CreateSpec extends Specification {

  import Namespaces.Default
  val DatasetId = "datastore-scala"

  "Google cloud datastore" should {
    "create an entity from tuples" in {
      val store = GoogleDatastore(DatasetId)
      val ent = store.create("Users", "name" -> "Dorel")
      Option(ent) must beSome
      ent.length must equalTo(32)
    }
    "create works with provided ID" in {
      val store = GoogleDatastore(DatasetId)
      val id = UUID.randomUUID.toString
      val ent = store.create("Users", store.IdProperty -> id, "name" -> "Dorel")
      Option(ent) must beSome
      ent must equalTo(id)
    }
    "transactionally create an entity from tuples" in {
      val store = GoogleDatastore(DatasetId)
      val ent = store.transactCreate("Users", "name" -> "Dorel")
      Option(ent) must beSome
      ent.length must equalTo(32)
    }
    "create an entity from map" in {
      val store = GoogleDatastore(DatasetId)
      val ent = store.create("Users", Map("name" -> "Dorel"))
      Option(ent) must beSome
      ent.length must equalTo(32)
    }
    "create an entity with auto id" in {
      val store = GoogleDatastore(DatasetId, AutoIdGenerator)
      val ent = store.create("Users", "name" -> "Dorel")
      ent must beGreaterThan(0l)
    }
    "create an entity with other namespace" in {
      val store = GoogleDatastore(DatasetId)
      val ent = store.create("Users", "name" -> "Dorel")(Namespace("test"))
      ent.length must equalTo(32)
    }
  }
}
