name := "datastore-scala"

libraryDependencies ++= Seq(
  "com.google.apis" % "google-api-services-datastore-protobuf" % "v1beta2-rev1-2.1.0" excludeAll(
    ExclusionRule("com.google.protobuf", "protobuf-java")),
    //ExclusionRule("org.codehaus.jackson", "jackson-core-asl")),
  "com.google.protobuf" % "protobuf-java" % "2.5.0"
)