package datastore.models

import com.google.api.services.datastore.client._
import com.google.api.services.datastore.DatastoreV1
import com.google.api.services.datastore.DatastoreV1._
import java.util.UUID
import java.util.Date
import com.google.protobuf.ByteString
import scala.annotation.tailrec

object GoogleDatastore {
  def apply[K](datasetId: String, gen: IdGenerator[K]): GoogleDatastore[K] = new GoogleDatastore(datasetId, gen)
  def apply(datasetId: String): GoogleDatastore[String] = apply(datasetId, new UUIDIdGenerator {})
}

case class Namespace(name: String)

object Namespaces {
  implicit val Default = Namespace("__DEFAULT_NAMESPACE__")
  def apply(n: String) = Namespace(n)
}

class GoogleDatastore[K](val datasetId: String, val gen: IdGenerator[K]) {

  lazy val datastore = init
  val isAutoGenerateId = gen.isInstanceOf[AutoIdGenerator]
  val IdProperty = "_id"
  val DefaultBatchSize = 10000
  //max delete batch for cloud datastore is 500
  val MaxDeleteBatchSize = 500

  def findAll(table: String)(implicit ns: Namespace) = {
    val toDelete = new java.util.LinkedList[Map[String, Any]]()
    foreach(table, e => toDelete add e)
    import scala.collection.JavaConversions._
    toDelete.toSeq
  }

  def foreach(table: String, rowProcessor: (Map[String, Any] => Unit), batchSize: Int = DefaultBatchSize)(implicit ns: Namespace) = {
    def nextBatch(startCursor: Option[ByteString] = None) = {
      import scala.collection.JavaConversions._
      val request = RunQueryRequest.newBuilder
      ns match {
        case Namespaces.Default => //nada
        case ns =>
          request.setPartitionId(PartitionId.newBuilder.setNamespace(ns.name))
      }
      val query = Query.newBuilder.setLimit(batchSize)
      startCursor.foreach(query.setStartCursor(_))
      query.addKindBuilder.setName(table)
      request.setQuery(query)
      val response = datastore.runQuery(request.build)
      val results = response.getBatch.getEntityResultList.toSeq.map(resultAsMap(_, true))
      results.foreach(rowProcessor)
      if (response.getBatch.getEntityResultCount < batchSize) //last batch!
        None else Option(response.getBatch.getEndCursor)
    }
    nextBatch() map { maybeCursor =>
      var x = Option(maybeCursor)
      do {
        x = nextBatch(x)
      } while (x.isDefined)
    }

  }

  def remove[K](table: String, ids: Seq[K], transactional: Boolean = false)(implicit ns: Namespace) {
    ids.grouped(MaxDeleteBatchSize).foreach { batch =>
      if (batch.size > 0) {
        val creq = CommitRequest.newBuilder
        val mutation = creq.getMutationBuilder
        batch.foreach(id => mutation.addDelete(makeKey(table, Option(id))))
        if (transactional) {
          creq.setTransaction(newTransaction)
          creq.setMode(CommitRequest.Mode.TRANSACTIONAL)
        } else {
          creq.setMode(CommitRequest.Mode.NON_TRANSACTIONAL)
        }
        val resp = datastore.commit(creq.build)
      }
    }
  }

  def remEntities(table: String, entities: Seq[Map[String, Any]], transactional: Boolean = false)(implicit ns: Namespace) {
    remove(table, entities.map(entity => entity(IdProperty)), transactional)
  }

  private def resultAsMap(r: EntityResult, includeId: Boolean = false): Map[String, Any] = {
    import scala.collection.JavaConversions._
    val props = DatastoreHelper.getPropertyMap(r.getEntity)
    val res = props.toMap.map { prop =>
      (prop._1, readValue(prop._2))
    }
    if (includeId) {
      res + (IdProperty -> r.getEntity.getKey.getPathElement(0).getName)
    } else res
  }

  def findOne(table: String, id: K)(implicit ns: Namespace): Option[Map[String, Any]] = {
    findOne(table, id, false)
  }

  def findOne(table: String, id: K, transactional: Boolean)(implicit ns: Namespace): Option[Map[String, Any]] = {
    val lreq = LookupRequest.newBuilder
    val key = makeKey(table, Option(id))
    lreq.addKey(key);
    if (transactional) {
      lreq.getReadOptionsBuilder().setTransaction(newTransaction)
    }
    val lresp = datastore.lookup(lreq.build);
    import scala.collection.JavaConversions._
    if (lresp.getFoundCount > 1) {
      throw new IllegalStateException(s"Found ${lresp.getFoundCount} of $table for key '$id'. Was expecting one or none.")
    }
    lresp.getFoundList.headOption.map(resultAsMap(_))
  }

  def create(table: String, data: (String, Any)*)(implicit ns: Namespace): K = {
    create(table, Map[String, Any](data: _*), false)
  }

  def transactCreate(table: String, data: (String, Any)*)(implicit ns: Namespace): K = {
    create(table, Map[String, Any](data: _*), true)
  }

  def create(table: String, data: Map[String, Any])(implicit ns: Namespace): K = {
    create(table, data, false)
  }

  def transactCreate(table: String, data: Map[String, Any])(implicit ns: Namespace): K = {
    create(table, data, true)
  }

  private def create(table: String, data: Map[String, Any], transactional: Boolean)(implicit ns: Namespace): K = {
    val entity = doBuild(table, data)
    val creq = CommitRequest.newBuilder
    val mutation = creq.getMutationBuilder()
    if (isAutoGenerateId) {
      mutation.addInsertAutoId(entity)
    } else {
      mutation.addInsert(entity)
    }
    if (transactional) {
      creq.setTransaction(newTransaction)
      creq.setMode(CommitRequest.Mode.TRANSACTIONAL)
    } else {
      creq.setMode(CommitRequest.Mode.NON_TRANSACTIONAL)
    }
    val resp = datastore.commit(creq.build)
    //laf, code below is deemed to blow
    if (isAutoGenerateId) {
      resp.getMutationResult.getInsertAutoIdKeyList.get(0).getPathElement(0).getId.asInstanceOf[K]
    } else {
      entity.getKey().getPathElement(0).getName.asInstanceOf[K]
    }
  }

  private def doBuild(table: String, data: Map[String, Any])(implicit ns: Namespace) = {
    val key = makeKey(table, data.get(IdProperty) orElse gen.nextId)
    val entityBuilder = Entity.newBuilder.setKey(key)
    data.keys.filter(_ != IdProperty).foreach { fldName =>
      makeValue(data(fldName)).foreach { value =>
        entityBuilder.addProperty(Property.newBuilder()
          .setName(fldName)
          .setValue(value))
      }
    }
    entityBuilder.build
  }

  private def makeKey[K](table: String, maybeId: Option[K])(implicit ns: Namespace) = {
    val keyBuilder = Key.PathElement.newBuilder().setKind(table)
    val id = maybeId match {
      case None => keyBuilder
      case Some(newId) => keyBuilder.setName(newId.toString)
    }
    val kindAndIdKey = Key.newBuilder.addPathElement(id)
    ns match {
      case Namespaces.Default => kindAndIdKey
      case any => kindAndIdKey
        .setPartitionId(PartitionId.newBuilder.setNamespace(ns.name))
    }
  }

  private def makeValue(value: Any): Option[DatastoreV1.Value.Builder] = {
    value match {
      case str: String => Some(DatastoreHelper.makeValue(str))
      case bool: Boolean => Some(DatastoreHelper.makeValue(bool))
      case lng: Long => Some(DatastoreHelper.makeValue(lng))
      case int: Integer => Some(DatastoreHelper.makeValue(int.toLong))
      case dbl: Double => Some(DatastoreHelper.makeValue(dbl))
      //TODO go JodaTime
      case dte: Date => Some(DatastoreHelper.makeValue(dte))
      case None => None
      case Some(any) => makeValue(any)
      case _ => throw new IllegalArgumentException(s"Unsupported value type: '$value'")
    }
  }

  private def readValue(value: Value): Any = {
    if (value.hasStringValue) {
      value.getStringValue
    } else if (value.hasIntegerValue) {
      value.getIntegerValue.toLong
    } else if (value.hasBooleanValue) {
      Boolean.box(value.getBooleanValue)
    } else if (value.hasDoubleValue) {
      value.getDoubleValue.toDouble
    } else if (value.hasTimestampMicrosecondsValue) {
      //TODO go JodaTime
      new java.util.Date(value.getTimestampMicrosecondsValue / 1000)
    } else {
      throw new IllegalStateException(s"Unsupported value read from datastore: $value")
    }
  }

  private def init = {
    DatastoreFactory.get.create(DatastoreHelper.getOptionsfromEnv
      .dataset(datasetId).build)
  }

  private def newTransaction = {
    val treq = BeginTransactionRequest.newBuilder
    val tres = datastore.beginTransaction(treq.build)
    tres.getTransaction
  }
}