package datastore.models

import java.util.UUID

trait IdGenerator[K] {
  def nextId: Option[K]
}

/**
 * Relies on google data store for ID generation
 */
trait AutoIdGenerator extends IdGenerator[Long] {
  override val nextId = None
}

object AutoIdGenerator extends AutoIdGenerator

trait UUIDIdGenerator extends IdGenerator[String] {
  override def nextId = Some(UUID.randomUUID.toString.replaceAll("-", ""))
}