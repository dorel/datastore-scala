name := "datastore-scala-play"

version in ThisBuild := "0.1.4"

organization in ThisBuild := "com.adaptivlabs"

libraryDependencies ++= Seq(
)     

publishMavenStyle in ThisBuild := true

publishTo in ThisBuild := {
  val localPublishRepo = "/Users/dorel/Work/_bitbucket_maven"
  Some(Resolver.file("releases", new File(localPublishRepo)))
}

play.Project.playScalaSettings

lazy val main = project.in(file("."))
    .aggregate(datastoreScala)
    .dependsOn(datastoreScala)

lazy val datastoreScala = project.in(file("zlibs/datastore-scala"))    
